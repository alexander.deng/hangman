require '../word'

RSpec.describe Word do
  let(:word_to_guess) {"october"}
  let(:word) { Word.new(word_to_guess) }

  describe "#contains_letter?" do
    context "when a guess is not in the word" do
      let(:letter) { "q" }

      it "returns false" do
        word.contains_letter?(letter).should == false
      end
    end

    context "when a guess is in the word" do
      let(:letter) { "o" }

      it "returns true" do
        word.contains_letter?(letter).should == true
      end
    end
  end

  describe "#letter_guessed? and #add_to_guessed_list" do
    context "letter o not guessed" do
      it "returns false" do
        word.letter_guessed?("o").should == false
      end
    end

    context "letter o guessed" do
      it "returns true" do
        word.add_to_guessed_list("o")
        word.letter_guessed?("o").should == true
      end
    end
  end

  describe "#expose_letter" do
    context "letter c exposed" do
      it "should return c" do
        word.expose_letter("c")
        word.exposed_letters[1].should == "c"
      end
    end
  end

  describe "#word_guessed?" do
    context "word not guessed" do
      it "should return false" do
        word.word_guessed?.should == false
      end
    end

    context "word guessed" do
      it "should return true" do
        word.expose_letter("o")
        word.expose_letter("c")
        word.expose_letter("t")
        word.expose_letter("b")
        word.expose_letter("e")
        word.expose_letter("r")
        word.word_guessed?.should == true
      end
    end
  end
end