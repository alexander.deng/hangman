require '../hangman'

RSpec.describe Hangman do
  let(:word_to_guess) {"october"}
  let(:hm) { Hangman.new(word_to_guess) }
  let(:correct_guess) {"o"}
  let(:incorrect_guess) {"i"}

  describe "#valid_input?" do
    context "input is valid" do
      it "should be valid" do
        expect(hm).to be_valid_input(correct_guess)
      end
    end

    context "input is invalid" do
      let(:invalid_guess) { 'qw1' }

      it "should be invalid" do
        expect(hm).not_to be_valid_input(invalid_guess)
      end
    end
  end

  describe "#game_over?" do
    context "game should not be over" do
      it "should return false" do
        expect(hm).not_to be_game_over
      end
    end

    context "game should be over" do
      let(:zero_lives_game) { Hangman.new(word_to_guess, 0) }
      it "should return true" do
        expect(zero_lives_game).to be_game_over
      end
    end
  end

  describe "#print_game_status" do
    context "when game is running" do
      it "prints lives remaining" do
        expect do
          hm.print_game_status
        end.to output(/^Lives remaining:.*/).to_stdout
      end
    end
  end

  describe "#evaluate_guess" do
    context "letter not guessed" do
      it "should print in word" do
        expect{hm.evaluate_guess(correct_guess)}.to output(/^#{correct_guess} in word.*/).to_stdout
      end
    end

    context "letter not in word" do
      it "should print not in word" do
        expect{hm.evaluate_guess(incorrect_guess)}.to output(/^#{incorrect_guess} not in word.*/).to_stdout
      end
    end

    context "letter already guessed" do
      it "should print already guessed" do
        hm.evaluate_guess(correct_guess)
        expect{hm.evaluate_guess(correct_guess)}.to output(/^#{correct_guess} has already been guessed.*/).to_stdout
      end
    end
  end

  describe "#start_game" do
    context "when game is over" do
      let(:one_life_game) { Hangman.new(word_to_guess, 1) }

      it "asks for letter" do
        allow(one_life_game).to receive(:gets).and_return(incorrect_guess)
        expect{one_life_game.start_game}.to output(/^Try a letter:.*/).to_stdout
      end

      it "prints game over" do
        allow(one_life_game).to receive(:gets).and_return(incorrect_guess)
        expect{one_life_game.start_game}.to output(/^Game Over!!.*/).to_stdout
      end

      it "prints word to guess " do
        allow(one_life_game).to receive(:gets).and_return(incorrect_guess)
        expect{one_life_game.start_game}.to output(/^Word to guess was: #{word_to_guess}.*/).to_stdout
      end
    end
  end
end