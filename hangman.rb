require './word'

class Hangman
  DEFAULT_NUMBER_OF_LIVES = 6

  attr_reader :word
  attr_accessor :lives_remaining

  def initialize(word, lives = DEFAULT_NUMBER_OF_LIVES)
    @word = Word.new(word)
    @lives_remaining = lives
  end

  def valid_input?(input)
    input.length == 1 && input.match(/[A-Za-z]/)
  end

  def game_over?
    lives_remaining == 0
  end

  def print_game_status
    puts `clear`
    puts "------------- Hangman -------------"
    puts word.exposed_letters.chars.join(" ")
    puts "Lives remaining: #{lives_remaining}"
    puts "Attempted letters: #{word.guessed_letters.join(" ")}"
  end

  def evaluate_guess(letter)
    if word.letter_guessed?(letter)
      puts "%s has already been guessed." % letter
    elsif word.contains_letter?(letter)
      puts "%s in word" % letter

      word.add_to_guessed_list(letter)
      word.expose_letter(letter)
    else
      puts "%s not in word" % letter

      word.add_to_guessed_list(letter)
      self.lives_remaining -= 1
    end
  end

  def start_game
    until game_over? || word.word_guessed?
      print_game_status

      puts "Hello"
      puts "Try a letter: "
      input = gets.chomp

      if valid_input?(input)
        letter = input.downcase

        evaluate_guess(letter)
      else
        puts "Invalid input. Please enter a character."
      end

      if game_over?
        puts "Game Over!!"
        puts "Word to guess was: #{word.word}"
      end

      if word.word_guessed?
        puts "Winner!"
      end

      sleep(0.5)
    end
  end
end

